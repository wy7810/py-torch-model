# Author: WY
# Date: 2023/3/28 20:35

import datetime
import os
import torch
import torch.nn as nn
import torch.optim as optim
import torch_geometric as PyG
import numpy as np
import networkx as nx
import matplotlib.pylab as plt
from pylab import mpl
from sklearn import metrics

# 防止plot时报错
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
# 设置显示中文字体
mpl.rcParams['font.sans-serif'] = ['SimHei']
# 设置正常显示符号
mpl.rcParams["axes.unicode_minus"] = False
# cuda
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# 文件地址
# pkl_path = 'result/model.pkl'
# val_path = 'result/val.txt'
# losses_img_path = 'result/img/losses.png'
# bias_img_path = 'result/img/bias.png'
# variance_img_path = 'result/img/variance.png'

# 图信息
node_num = 21
edges = [
    (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7),
    (2, 8), (3, 9), (4, 10), (4, 11), (5, 12), (6, 13), (7, 14), (7, 15), (7, 16),
    (11, 17), (12, 18), (13, 19), (15, 20)
]
weights = [
    0.5943, 0.8628, 0.8132, 0.6310, 0.8598, 0.6794, 0.5088,
    0.8638, 0.8536, 0.6448, 0.3974, 0.4254, 0.5618, 0.7458, 0.7001, 0.7193,
    0.6083, 0.7181, 0.7206, 0.7835
]

# 超参数
scopes = 100 # 200
lr = 0.0002 # 0.0003
split = 0.8 # 0.9/0.8
dropout = 0.2 # 0.1
weight_decay = 5e-4
batch_size = 40 # 120      40  60  100  120
# pre_step = 8 # 8            1-2 3-4 5-6 7-8
feature_dim = 13
temp_size_conv1 = 13 # 13
temp_size_conv2 = 13 # 13
grad = 2
gcn_output_size = 7 # 7
output_size = 1
hidden_size = 20 # 20
num_layers = 1 # 1
# attention参数
# num_heads = 1


class GCN_LSTM(torch.nn.Module):
    def __init__(self, step):
        super(GCN_LSTM, self).__init__()
        # 预测步长
        self.pre_step = step
        # 第一层图卷积
        self.conv1 = PyG.nn.GCNConv(
            in_channels=feature_dim,
            out_channels=temp_size_conv1
        )
        # 第二层图卷积
        self.conv2 = PyG.nn.GCNConv(
            in_channels=temp_size_conv1,
            out_channels=temp_size_conv2
        )
        # Dropout
        self.dropout = nn.Dropout(dropout)
        # 多层感知机
        self.MLP = nn.Sequential(
            nn.Linear(temp_size_conv2, (temp_size_conv2 + gcn_output_size) // grad),
            nn.ReLU(inplace=True),
            nn.Linear((temp_size_conv2 + gcn_output_size) // grad, gcn_output_size)
        )
        # 全连接层(用于结合各站点之间的空间关系)
        self.FC1 = nn.Linear(node_num, 1)
        # ReLU
        self.relu1 = nn.ReLU()
        # Attention
        # self.attention = nn.MultiheadAttention(feature_dim + gcn_output_size, num_heads)
        # LSTM层
        self.lstm = nn.LSTM(
            input_size=feature_dim + gcn_output_size,
            hidden_size=hidden_size,
            num_layers=num_layers,
            batch_first=True,
            bidirectional=True
        )
        # 初始化LSTM的各参数
        for p in self.lstm.parameters():
            nn.init.normal_(p, mean=0.0, std=0.001)
        # 全连接层(获取序列预测结果)
        self.FC2 = nn.Linear(hidden_size * 2, output_size)
        # 归纳结果
        self.FC3 = nn.Linear(batch_size, self.pre_step)

    def forward(self, input, edge_index, edge_weight):
        # 图卷积,提取空间关系  [batch_size, node_num, feature_dim] -> [batch_size, node_num, temp_size_conv2]
        out = self.conv1(input, edge_index, edge_weight)
        out = self.conv2(out, edge_index, edge_weight)
        # 防止过拟合 ReLU + Dropout
        out = self.relu1(out)
        out = self.dropout(out)
        # 多层感知机,整合各站点的各自特征数据为指定维度的特征张量  [batch, node_num, temp_size_conv2] -> [batch, node_num, gcn_output_size]
        out = self.MLP(out)
        # 置换站点与特征数的维度  [batch, node_num, gcn_output_size] -> [batch, gcn_output_size, node_num]
        out = out.transpose(1, 2)
        # 整合t时刻所有站点数据到一个维度  [batch, gcn_output_size, node_num] -> [batch, gcn_output_size, 1]
        out = self.FC1(out)
        # 将结果降维 [batch, gcn_output_size, 1] -> [batch, gcn_output_size]
        out = torch.squeeze(out, dim=2)

        # 取出主数据部分
        main_data = input[:, 0, :]
        # 合并空间张量到主数据的时序特征数据中  [batch, feature_dim + gcn_output_size]
        out = torch.cat([main_data, out], dim=1)
        # GCN_out
        # print(f"GCN_out: {out.shape}")

        # Attention [batch, hidden_size] 输出结构保持不变
        # query, key, value = out, out, out
        # out, _ = self.attention(query, key, value)

        # 带入LSTM层  [batch, features] -> [batch, hidden_size]
        out, _ = self.lstm(out)

        # [batch, hidden_size] -> [batch, output_size]
        out = self.FC2(out)
        # [batch, output_size] -> [output_size, batch] -> [output_size, pre_step]
        out = self.FC3(out.T)

        return out


def test(dataloader_train, step):
    model = GCN_LSTM(step)
    # 通过networkx绘制图,并提取邻接表
    graph = nx.Graph()
    graph.add_nodes_from(np.array(range(node_num)))
    graph.add_edges_from(edges)
    # 制作邻接表和权重表
    edge_index = torch.LongTensor(np.array([edge for edge in graph.edges]).T)
    edge_weight = torch.FloatTensor(weights)

    data = next(iter(dataloader_train))
    # 应GCN的数据类型要求,将 float64(Float) -> float32(Double)
    data = data.to(torch.float32)
    # input与label
    input = data[0:batch_size, :, :]
    label = data[batch_size:, 0, 0].view(1, step)
    print(f"input: {input.shape}")
    print(f"label: {label.shape}")
    # 带入模型
    out = model(input, edge_index, edge_weight)
    print(f"out: {out.shape}")


def train(dataloader_train, step, pkl_path, losses_img_path, val_path):
    losses = []
    loss_record = []
    # 实例化模型,损失函数和优化器
    model = GCN_LSTM(step).to(device)
    loss_fun = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    # 通过networkx绘制图,并提取邻接表
    graph = nx.Graph()
    graph.add_nodes_from(np.array(range(node_num)))
    graph.add_edges_from(edges)
    # 制作邻接表和权重表
    edge_index = torch.LongTensor(np.array([edge for edge in graph.edges]).T).to(device)
    edge_weight = torch.FloatTensor(weights).to(device)
    # 加载训练结果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 记录启动时间
    start_date = datetime.datetime.now()
    print(f"start {start_date}")
    # 开始训练
    for scope in range(scopes):
        for batch_idx, (data) in enumerate(dataloader_train):
            # 应GCN的数据类型要求,将 float64(Float) -> float32(Double)
            data = data.to(torch.float32)
            # input与label
            input = data[0:batch_size, :, :].to(device)
            label = data[batch_size:, 0, 0].view(1, step).to(device)
            # 带入模型
            out = model(input, edge_index, edge_weight)
            # loss与backward
            loss = loss_fun(out, label)
            model.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 10 == 0:
                losses.append(loss.item())

        sum = np.sum(losses)
        loss_record.append(sum)
        print(f"scope: {scope}, losses: {sum}")
        losses.clear()

    # 记录结束时间
    end_date = datetime.datetime.now()
    print(f"end {end_date}")
    # 保存训练参数结果,如果目标地址父文件夹不存在就先创建再保存,存在就跳过创建直接保存
    os.makedirs(os.path.dirname(pkl_path), exist_ok=True)
    torch.save(model.state_dict(), pkl_path)
    # 损失函数图像
    fig = plt.figure()
    plt.plot(np.array(range(len(loss_record))), loss_record)
    plt.title(label=f'损失值变化 训练耗时:{end_date - start_date}')
    plt.show()
    # 保存图像
    fig.savefig(losses_img_path)
    # 打印損失值
    np.savetxt(val_path, loss_record, fmt="%.7f", delimiter=',')


def bias_test(dataloader_train, step, pkl_path, bias_img_path):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,制作邻接表
    model = GCN_LSTM(step).to(device)
    graph = nx.Graph()
    graph.add_nodes_from(np.array(range(node_num)))
    graph.add_edges_from(edges)
    edge_index = torch.LongTensor(np.array([edge for edge in graph.edges]).T).to(device)
    edge_weight = torch.FloatTensor(weights).to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_train):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:batch_size, :, :].to(device)
        label = data[batch_size:, 0, 0].view(1, step).tolist()[0]
        # 带入模型,得到结果
        out = model(input, edge_index, edge_weight)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label='模型偏差')
    plt.show()
    # 保存图像
    fig.savefig(bias_img_path)


def variance_test(dataloader_test, step, pkl_path, variance_img_path):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,制作邻接表
    model = GCN_LSTM(step).to(device)
    graph = nx.Graph()
    graph.add_nodes_from(np.array(range(node_num)))
    graph.add_edges_from(edges)
    edge_index = torch.LongTensor(np.array([edge for edge in graph.edges]).T).to(device)
    edge_weight = torch.FloatTensor(weights).to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_test):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:batch_size, :, :].to(device)
        label = data[batch_size:, 0, 0].view(1, step).tolist()[0]
        # 带入模型,得到结果
        out = model(input, edge_index, edge_weight)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    # RMSE
    RMSE = np.sqrt(metrics.mean_squared_error(yAxis_label, yAxis_predict))
    # MAE
    MAE = metrics.mean_absolute_error(yAxis_label, yAxis_predict)
    # 绘图
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label=f"模型方差 RMSE={RMSE} MAE={MAE}")
    plt.legend(loc='upper right')
    plt.show()
    # 保存图像
    fig.savefig(variance_img_path)


def performance_record(step, dataloader_train, dataloader_test):
    print(f"正在处理预测步长为{step}的模型流程")
    # 动态地址
    pkl_path = f'result/pre_step = {step}/results.pkl'
    losses_img_path = f'result/pre_step = {step}/losses.png'
    bias_img_path = f'result/pre_step = {step}/bias.png'
    variance_img_path = f'result/pre_step = {step}/variance.png'
    val_path = f'result/pre_step = {step}/val.txt'
    # 带入模型流程,记录结果
    train(dataloader_train, step, pkl_path, losses_img_path, val_path)
    bias_test(dataloader_train, step, pkl_path, bias_img_path)
    variance_test(dataloader_test, step, pkl_path, variance_img_path)


