# Author: WY
# Date: 2022/11/10 11:09

import datetime
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pylab as plt
import os
from pylab import mpl
import os
import pickle

# 防止plot时报错
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
# 设置显示中文字体
mpl.rcParams['font.sans-serif'] = ['SimHei']
# 设置正常显示符号
mpl.rcParams["axes.unicode_minus"] = False
# cuda
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# 超参数
batch_size = 4
seq_len = 3
input_size = 4
output_size = 1
hidden_size = 10
num_layers = 1
scopes = 100
lr = 0.0001
split = 0.7
# 模型文件路径
pkl_path = 'result/temperature/results.pkl'


# 模型
class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        self.gru = nn.GRU(
            input_size=input_size,
            hidden_size=hidden_size,
            num_layers=num_layers,
            batch_first=True
        )
        for p in self.gru.parameters():
            nn.init.normal_(p, mean=0.0, std=0.001)
        # 全连接层
        self.linear = nn.Linear(hidden_size, output_size)

    def forward(self, input):
        # 初始化h0，c0  [num_layers, batch, hidden_size]
        h_prev = torch.zeros(num_layers, 1, hidden_size).to(device)
        out, _ = self.lstm(input, h_prev)
        # [batch, seq_len, hidden_size] => [seq_len, hidden_size]
        out = out.view(-1, hidden_size)
        # [seq_len, hidden_size] => [seq_len, output_size]
        out = self.linear(out)
        # [seq_len, output_size] => [1, seq_len, output_size]
        out = out.unsqueeze(dim=0)
        # 前面的数据都融入到了最后一排,只要最后一排的out的数据
        out = torch.index_select(out, 1, torch.tensor([seq_len - 1]).to(device))
        return out


# 训练
def train(dataloader_train):
    losses = []
    # 实例化模型,损失函数,优化器
    model = Model().to(device)
    loss_fun = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))

    # 开始训练
    for scope in range(scopes):
        for batch_idx, (data) in enumerate(dataloader_train):
            # [batch, seq_len, input_size]
            input = data[0:seq_len].float().view(1, seq_len, input_size).to(device)
            # [batch, batch_size - seq_len, output_size]
            label = data[seq_len][0].float().view(1, batch_size - seq_len, output_size).to(device)

            output = model(input)
            loss = loss_fun(output, label)
            model.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 100 == 0:
                losses.append(loss.item())
                print(f"batch_idx: {batch_idx}, out: {output[0][0][0]}, label: {label[0][0][0]}, loss: {loss.item()}")

        print(f"scope: {scope}, losses: {losses}")
        losses.clear()

    # 保存训练参数结果
    torch.save(model.state_dict(), pkl_path)

    return model


# 用训练集测模型偏差
def bias_test(dataloader_train):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,加载训练成果
    model = Model().to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))

    for batch_idx, (data) in enumerate(dataloader_train):
        input = data[0:seq_len].float().view(1, seq_len, input_size).to(device)
        label = data[seq_len][0].numpy()

        output = model(input)

        output = output.cpu().detach()
        outputs.append(output[0][0][0])
        labels.append(label)
        print(f"output: {output[0][0][0]}, label: {label}")

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    plt.plot(xAxis, yAxis_label, 'r-.', label='label')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='predict')
    plt.title(label='模型偏差')
    plt.show()


# 用测试集测模型方差
def variance_test(dataloader_test):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,加载训练成果
    model = Model().to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))

    for batch_idx, (data) in enumerate(dataloader_test):
        input = data[0:seq_len].float().view(1, seq_len, input_size).to(device)
        label = data[seq_len][0].numpy()

        output = model(input)

        output = output.cpu().detach()
        outputs.append(output[0][0][0])
        labels.append(label)
        print(f"output: {output[0][0][0]}, label: {label}")

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    plt.plot(xAxis, yAxis_label, 'r-.', label='label')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='predict')
    plt.title(label='模型方差')
    plt.show()
