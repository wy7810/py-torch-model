# Author: WY
# Date: 2022/11/10 11:09

import numpy as np
import model.GRU_Model as GRU
import model.DataLoader as loader
import pandas as pd

if __name__ == '__main__':
    # 获取数据集
    # dataframe = pd.read_csv('./data/temperature/climate.csv')
    dataframe = pd.read_csv('./data/temperature/jena_climate_2009_2016.csv')

    # data = np.array(dataframe[['Temperature']])
    # data = np.array(dataframe[['Average temperature', 'Average humidity', 'Average dewpoint', 'Average barometer', 'Average windspeed', 'Average gustspeed']])
    data = np.array(dataframe[['Temperature', 'Temperature in Kelvin', 'Temperature (dew point)', 'Pressure',
                               'Relative Humidity', 'Saturation vapor pressure', 'Vapor pressure',
                               'Vapor pressure deficit', 'Specific humidity', 'Water vapor concentration', 'Airtight',
                               'Wind speed', 'Maximum wind speed', 'Wind direction in degrees']])

    # dataloader_train, dataloader_test = loader.get_loader(split=GRU.split, batch_size=GRU.batch_size, data=data)
    dataloader_train, dataloader_test = loader.get_loader(split=0.992, batch_size=GRU.batch_size, data=data)

    # GRU.train(dataloader_train)
    GRU.bias_test(dataloader_train)
    GRU.variance_test(dataloader_test)
