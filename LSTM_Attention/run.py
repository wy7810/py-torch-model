# Author: WY
# Date: 2023/7/13 15:12

import numpy as np
import model.LSTM_Att_Model as lstm_model
import model.DataLoader as loader
import pandas as pd

if __name__ == '__main__':
    # 获取数据集
    dataframe = pd.read_csv('./data/p_56187(成都市)_01.01.2006_01.01.2023.csv')

    data = np.array(dataframe[['大气温度（摄氏度）', '水平大气压（毫米汞柱）', '海平面大气压（毫米汞柱）', '气压变化趋势', '相对湿度', '风向', '平均风速（m/s）', '过去12小时内最低气温', '过去12小时内最高气温', '水平能见度（km）', '露点温度（摄氏度）', '降水量（毫米）', '到达规定降水量的时间']])

    # 测试模型可行性
    # dataloader_train, dataloader_test = loader.get_loader(split=lstm_model.split, batch_size=lstm_model.batch_size + 8, data=data)
    # lstm_model.test(dataloader_train, 8)

    # 连续预测步长性能记录
    for step in range(10, 11):
        dataloader_train, dataloader_test = loader.get_loader(split=lstm_model.split, batch_size=lstm_model.batch_size + step, data=data)
        lstm_model.performance_record(step, dataloader_train, dataloader_test)



