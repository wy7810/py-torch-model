# Author: WY
# Date: 2023/4/20 15:52

import datetime
import os
import torch
import torch.nn as nn
import torch.optim as optim
import torch_geometric as PyG
import numpy as np
import networkx as nx
import matplotlib.pylab as plt
from pylab import mpl
from sklearn import metrics

# 防止plot时报错
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
# 设置显示中文字体
mpl.rcParams['font.sans-serif'] = ['SimHei']
# 设置正常显示符号
mpl.rcParams["axes.unicode_minus"] = False
# cuda
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# 文件地址
# pkl_path = 'result/model.pkl'
# val_path = 'result/val.txt'
# losses_img_path = 'result/img/losses.png'
# bias_img_path = 'result/img/bias.png'
# variance_img_path = 'result/img/variance.png'

# 超参数
scopes = 40
lr = 0.0002 # 0.0003
split = 0.8 # 0.9
dropout = 0.2
weight_decay = 5e-4
batch_size = 40 # 120
# pre_step = 8 # 8
feature_dim = 13
output_size = 1
hidden_size = 39 # 20
num_layers = 3 # 1


class GCN_LSTM(torch.nn.Module):
    def __init__(self, step):
        super(GCN_LSTM, self).__init__()

        # LSTM层
        self.lstm = nn.LSTM(
            input_size=feature_dim,
            hidden_size=hidden_size,
            num_layers=num_layers,
            batch_first=True,
            # bidirectional=True
        )
        for p in self.lstm.parameters():
            nn.init.normal_(p, mean=0.0, std=0.001)
        # 全连接层(获取序列预测结果)
        # self.FC2 = nn.Linear(hidden_size * 2, output_size)
        self.FC2 = nn.Linear(hidden_size, output_size)
        # 归纳结果
        self.FC3 = nn.Linear(batch_size, step)

    def forward(self, input):

        # 带入LSTM层  [batch, features] -> [batch, hidden_size]
        out, _ = self.lstm(input)
        # [batch, hidden_size] -> [batch, output_size]
        out = self.FC2(out)
        # [batch, output_size] -> [output_size, batch] -> [output_size, pre_step]
        out = self.FC3(out.T)
        return out


def train(dataloader_train, step, pkl_path, losses_img_path, val_path):
    losses = []
    loss_record = []

    # 实例化模型,损失函数和优化器
    model = GCN_LSTM(step).to(device)
    loss_fun = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)

    # 加载训练结果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 记录启动时间
    start_date = datetime.datetime.now()
    print(f"start {start_date}")
    # 开始训练
    for scope in range(scopes):
        for batch_idx, (data) in enumerate(dataloader_train):
            # 应GCN的数据类型要求,将 float64 -> float32
            data = data.to(torch.float32)
            # input与label
            input = data[0:batch_size, :].to(device)
            label = data[batch_size:, 0].view(1, step).to(device)
            # 带入模型
            out = model(input)
            # loss与backward
            loss = loss_fun(out, label)
            model.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 10 == 0:
                losses.append(loss.item())

        sum = np.sum(losses)
        loss_record.append(sum)
        print(f"scope: {scope}, losses: {sum}")
        losses.clear()

    # 记录结束时间
    end_date = datetime.datetime.now()
    print(f"end {end_date}")
    # 保存训练参数结果,如果目标地址父文件夹不存在就先创建再保存,存在就跳过创建直接保存
    os.makedirs(os.path.dirname(pkl_path), exist_ok=True)
    torch.save(model.state_dict(), pkl_path)
    # 损失函数图像
    fig = plt.figure()
    plt.plot(np.array(range(len(loss_record))), loss_record)
    plt.title(label=f'损失值变化 训练耗时:{end_date - start_date}')
    plt.show()
    # 保存图像
    fig.savefig(losses_img_path)
    # 打印損失值
    np.savetxt(val_path, loss_record, fmt="%.7f", delimiter=',')


def bias_test(dataloader_train, step, pkl_path, bias_img_path):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例
    model = GCN_LSTM(step).to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_train):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:batch_size, :].to(device)
        label = data[batch_size:, 0].view(1, step).tolist()[0]
        # 带入模型,得到结果
        out = model(input)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label='模型偏差')
    plt.show()
    # 保存图像
    fig.savefig(bias_img_path)


def variance_test(dataloader_test, step, pkl_path, variance_img_path):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,制作邻接表
    model = GCN_LSTM(step).to(device)

    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_test):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:batch_size, :].to(device)
        label = data[batch_size:, 0].view(1, step).tolist()[0]
        # 带入模型,得到结果
        out = model(input)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    # RMSE
    RMSE = np.sqrt(metrics.mean_squared_error(yAxis_label, yAxis_predict))
    # MAE
    MAE = metrics.mean_absolute_error(yAxis_label, yAxis_predict)
    # 绘图
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label=f"模型方差 RMSE={RMSE} MAE={MAE}")
    plt.legend(loc='upper right')
    plt.show()
    # 保存图像
    fig.savefig(variance_img_path)


def performance_record(step, dataloader_train, dataloader_test):
    print(f"正在处理预测步长为{step}的模型流程")
    # 动态地址
    pkl_path = f'result/temperature/pre_step = {step}/results.pkl'
    losses_img_path = f'result/temperature/pre_step = {step}/losses.png'
    bias_img_path = f'result/temperature/pre_step = {step}/bias.png'
    variance_img_path = f'result/temperature/pre_step = {step}/variance.png'
    val_path = f'result/temperature/pre_step = {step}/val.txt'
    # 带入模型流程,记录结果
    train(dataloader_train, step, pkl_path, losses_img_path, val_path)
    bias_test(dataloader_train, step, pkl_path, bias_img_path)
    variance_test(dataloader_test, step, pkl_path, variance_img_path)

