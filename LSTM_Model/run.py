# Author: WY
# Date: 2022/11/10 11:09

import numpy as np
import model.LSTM_Model as LSTM
import model.BiLSTM_Model as BiLSTM
import model.DataLoader as loader
import pandas as pd

if __name__ == '__main__':
    # 获取数据集
    # dataframe = pd.read_csv('./data/temperature/jena_climate_2009_2016.csv')
    dataframe = pd.read_csv('./data/temperature/p_56187(成都市)_01.01.2006_01.01.2023.csv')

    # data = np.array(dataframe[
    #                     ['Temperature', 'Temperature in Kelvin', 'Temperature (dew point)', 'Relative Humidity',
    #                      'Pressure',
    #                      'Saturation vapor pressure', 'Vapor pressure', 'Vapor pressure deficit', 'Specific humidity',
    #                      'Water vapor concentration', 'Airtight', 'Wind speed', 'Maximum wind speed',
    #                      'Wind direction in degrees']])
    data = np.array(dataframe[["大气温度（摄氏度）", "水平大气压（毫米汞柱）", "海平面大气压（毫米汞柱）", "气压变化趋势", "相对湿度", "风向", "平均风速（m/s）", "过去12小时内最低气温", "过去12小时内最高气温", "水平能见度（km）", "露点温度（摄氏度）", "降水量（毫米）", "到达规定降水量的时间"]])

    # dataloader_train, dataloader_test = loader.get_data(split=LSTM.split, batch_size=LSTM.batch_size, data=data)

    for step in range(1, 21):
        dataloader_train, dataloader_test = loader.get_data(split=BiLSTM.split, batch_size=BiLSTM.batch_size + step, data=data)
        BiLSTM.performance_record(step, dataloader_train, dataloader_test)

    # LSTM.train(dataloader_train)
    # LSTM.bias_test(dataloader_train)
    # LSTM.variance_test(dataloader_test)

