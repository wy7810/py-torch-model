# Author: WY
# Date: 2023/5/8 16:08

import datetime
import os
import torch
import torch.nn as nn
from torch.nn.utils import weight_norm
import torch.optim as optim
import torch_geometric as PyG
import numpy as np
import networkx as nx
import matplotlib.pylab as plt
from pylab import mpl
from sklearn import metrics

# 防止plot时报错
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"
# 设置显示中文字体
mpl.rcParams['font.sans-serif'] = ['SimHei']
# 设置正常显示符号
mpl.rcParams["axes.unicode_minus"] = False
# cuda
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# 文件地址
pkl_path = 'result/model.pkl'
val_path = 'result/val.txt'
losses_img_path = 'result/img/losses.png'
bias_img_path = 'result/img/bias.png'
variance_img_path = 'result/img/variance.png'

# 超参数
scopes = 50
lr = 0.0002 # 0.0003
split = 0.9 # 0.9
weight_decay = 5e-4
feature_dim = 13
batch_size = 40
num_outputs = 2
num_inputs = batch_size - num_outputs
num_channels = [50, 50, 50, 50] # [45, 45, 45, 45]
kernel_size = 3
stride = 1
dropout = 0.2


class Chomp1d(nn.Module):
    def __init__(self, chomp_size):
        super(Chomp1d, self).__init__()
        self.chomp_size = chomp_size

    def forward(self, x):
        # return x[:, :, :-self.chomp_size].contiguous()
        return x[:, :-self.chomp_size].contiguous()


class TemporalBlock(nn.Module):
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation, padding, dropout=0.2):
        """
        相当于一个Residual block

        :param n_inputs: int, 输入通道数
        :param n_outputs: int, 输出通道数
        :param kernel_size: int, 卷积核尺寸
        :param stride: int, 步长，一般为1
        :param dilation: int, 膨胀系数
        :param padding: int, 填充系数
        :param dropout: float, dropout比率
        """
        super(TemporalBlock, self).__init__()
        # weight_norm: 权重归一化
        # self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size, stride=stride, padding=padding, dilation=dilation))
        self.conv1 = weight_norm(nn.Conv2d(n_inputs, n_outputs, (1, kernel_size), stride=stride, padding=(0, padding), dilation=dilation))
        self.softmax1 = nn.Softmax(dim=0)
        # 经过conv1，输出的size其实是(Batch, input_channel, seq_len + padding)
        self.chomp1 = Chomp1d(padding)  # 裁剪掉多出来的padding部分，维持输出时间步为seq_len
        self.relu1 = nn.ReLU(inplace=True)
        self.dropout1 = nn.Dropout(dropout)

        # self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size, stride=stride, padding=padding, dilation=dilation))
        self.conv2 = weight_norm(nn.Conv2d(n_outputs, n_outputs, (1, kernel_size), stride=stride, padding=(0, padding), dilation=dilation))
        self.softmax2 = nn.Softmax(dim=0)
        self.chomp2 = Chomp1d(padding)  # 裁剪掉多出来的padding部分，维持输出时间步为seq_len
        self.relu2 = nn.ReLU(inplace=True)
        self.dropout2 = nn.Dropout(dropout)

        self.net = nn.Sequential(self.conv1, self.softmax1, self.chomp1, self.relu1, self.dropout1,
                                 self.conv2, self.softmax2, self.chomp2, self.relu2, self.dropout2)
        # 卷积核为1x1的卷积过程,在输入TemporalBlock时数据的通道数与输出时的通道数不等时使用,计算残差
        self.downsample = nn.Conv1d(n_inputs, n_outputs, 1) if n_inputs != n_outputs else None
        self.relu = nn.ReLU(inplace=True)
        self.init_weights()

    def init_weights(self):
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample is not None:
            self.downsample.weight.data.normal_(0, 0.01)

    def forward(self, x):
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        return self.relu(out + res)


class TemporalConvNet(nn.Module):
    def __init__(self):
        """
        TCN，目前paper给出的TCN结构很好的支持每个时刻为一个数的情况，即sequence结构，
        对于每个时刻为一个向量这种一维结构，勉强可以把向量拆成若干该时刻的输入通道，
        对于每个时刻为一个矩阵或更高维图像的情况，就不太好办。

        :param num_inputs: int， 输入通道数
        :param num_channels: list，每层的hidden_channel数，例如[25,25,25,25]表示有4个隐层，每层hidden_channel数为25
        :param kernel_size: int, 卷积核尺寸
        :param dropout: float, drop_out比率
        """
        super(TemporalConvNet, self).__init__()
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i  # 膨胀系数：1，2，4，8……
            in_channels = num_inputs if i == 0 else num_channels[i - 1]  # 确定每一层的输入通道数
            out_channels = num_channels[i]  # 确定每一层的输出通道数
            layers += [TemporalBlock(in_channels, out_channels, kernel_size, stride=stride, dilation=dilation_size, padding=(kernel_size - 1) * dilation_size, dropout=dropout)]

        self.network = nn.Sequential(*layers)

    def forward(self, x):
        """
        输入x的结构不同于RNN，一般RNN的size为(Batch, seq_len, channels)或者(seq_len, Batch, channels)，
        这里把seq_len放在channels后面，把所有时间步的数据拼起来，当做Conv1d的输入尺寸，实现卷积跨时间步的操作，
        很巧妙的设计。

        :param x: size of (Batch, input_channel, seq_len)
        :return: size of (Batch, output_channel, seq_len)
        """
        return self.network(x).T


class TCN(nn.Module):
    def __init__(self):
        super(TCN, self).__init__()
        self.tcn = TemporalConvNet()
        self.fc = nn.Linear(num_channels[-1], num_outputs)

    def forward(self, x):
        # [channel, height, width]
        x = x.reshape([1, num_inputs, feature_dim])
        out = self.tcn(x)
        out = self.fc(out)
        return out


def test(dataloader_train):
    model = TCN()

    data = next(iter(dataloader_train))
    # 应GCN的数据类型要求,将 float64(Float) -> float32(Double)
    data = data.to(torch.float32)
    # input与label
    input = data[0:(batch_size - num_outputs), :]
    label = data[(batch_size - num_outputs):, 0].view(1, num_outputs)
    print(f"input: {input.shape}")
    print(f"label: {label.shape}")

    out = model(input)
    print(f"out: {out.shape}")

def train(dataloader_train):
    losses = []
    loss_record = []
    # 实例化模型,损失函数和优化器
    model = TCN().to(device)
    loss_fun = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)
    # 加载训练结果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 记录启动时间
    start_date = datetime.datetime.now()
    print(f"start {start_date}")
    # 开始训练
    for scope in range(scopes):
        for batch_idx, (data) in enumerate(dataloader_train):
            # 应GCN的数据类型要求,将 float64(Float) -> float32(Double)
            data = data.to(torch.float32)
            # input与label
            input = data[0:(batch_size - num_outputs), :].to(device)
            label = data[(batch_size - num_outputs):, 0].view(1, num_outputs).to(device)
            # 带入模型
            out = model(input)
            # loss与backward
            loss = loss_fun(out, label)
            model.zero_grad()
            loss.backward()
            optimizer.step()

            if batch_idx % 10 == 0:
                losses.append(loss.item())

        sum = np.sum(losses)
        loss_record.append(sum)
        print(f"scope: {scope}, losses: {sum}")
        losses.clear()

    # 记录结束时间
    end_date = datetime.datetime.now()
    print(f"end {end_date}")
    # 保存模型训练结果
    torch.save(model.state_dict(), pkl_path)
    # 损失函数图像
    fig = plt.figure()
    plt.plot(np.array(range(len(loss_record))), loss_record)
    plt.title(label=f'损失值变化 训练耗时:{end_date - start_date}')
    plt.show()
    # 保存图像
    fig.savefig(losses_img_path)
    # 打印損失值
    np.savetxt(val_path, loss_record, fmt="%.7f", delimiter=',')


def bias_test(dataloader_train):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,制作邻接表
    model = TCN().to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_train):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:(batch_size - num_outputs), :].to(device)
        label = data[(batch_size - num_outputs):, 0].view(1, num_outputs).tolist()[0]
        # 带入模型,得到结果
        out = model(input)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label='模型偏差')
    plt.show()
    # 保存图像
    fig.savefig(bias_img_path)


def variance_test(dataloader_test):
    # 接受array
    outputs = []
    labels = []
    # 获取模型实例,制作邻接表
    model = TCN().to(device)
    # 加载训练成果
    if os.path.exists(pkl_path):
        model.load_state_dict(torch.load(pkl_path))
    # 预测结果
    for batch_idx, (data) in enumerate(dataloader_test):
        # 应GCN的数据类型要求,将 float64 -> float32
        data = data.to(torch.float32)
        # input和label
        input = data[0:(batch_size - num_outputs), :].to(device)
        label = data[(batch_size - num_outputs):, 0].view(1, num_outputs).tolist()[0]
        # 带入模型,得到结果
        out = model(input)
        # 取出预测值
        output = out.cpu().detach().tolist()[0]
        # 记录结果
        outputs.extend(output)
        labels.extend(label)

    # 打印预测结果和label
    xAxis = np.array(range(len(outputs)))
    yAxis_predict = np.array(outputs)
    yAxis_label = np.array(labels)
    # RMSE
    RMSE = np.sqrt(metrics.mean_squared_error(yAxis_label, yAxis_predict))
    # MAE
    MAE = metrics.mean_absolute_error(yAxis_label, yAxis_predict)
    # 绘图
    fig = plt.figure()
    plt.plot(xAxis, yAxis_label, 'r-.', label='真实值')
    plt.plot(xAxis, yAxis_predict, 'b-.', label='预测值')
    plt.title(label=f"模型方差 RMSE={RMSE} MAE={MAE}")
    plt.legend(loc='upper right')
    plt.show()
    # 保存图像
    fig.savefig(variance_img_path)
