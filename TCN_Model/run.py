# Author: WY
# Date: 2023/5/8 16:07

import numpy as np
import model.TCN_Model as TCN
import model.TCN_2D as TCN_2D
import model.test_model as testModel
import model.DataLoader as loader
import pandas as pd

if __name__ == '__main__':
    # 获取数据集
    dataframe = pd.read_csv('./data/p_56187(成都市)_01.01.2006_01.01.2023.csv')

    data = np.array(dataframe[["大气温度（摄氏度）", "水平大气压（毫米汞柱）", "海平面大气压（毫米汞柱）", "气压变化趋势", "相对湿度", "风向", "平均风速（m/s）", "过去12小时内最低气温", "过去12小时内最高气温", "水平能见度（km）", "露点温度（摄氏度）", "降水量（毫米）", "到达规定降水量的时间"]])

    dataloader_train, dataloader_test = loader.get_loader(split=testModel.split, batch_size=testModel.batch_size, data=data)

    # testModel.test(dataloader_train)

    testModel.train(dataloader_train)
    testModel.bias_test(dataloader_train)
    testModel.variance_test(dataloader_test)

    # TCN_2D.test(dataloader_train)

    # TCN_2D.train(dataloader_train)
    # TCN_2D.bias_test(dataloader_train)
    # TCN_2D.variance_test(dataloader_test)

    # TCN.test(dataloader_train)

    # TCN.train(dataloader_train)
    # TCN.bias_test(dataloader_train)
    # TCN.variance_test(dataloader_test)




